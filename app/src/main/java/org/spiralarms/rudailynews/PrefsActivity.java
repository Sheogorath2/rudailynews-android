package org.spiralarms.rudailynews;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonWriter;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONStringer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrefsActivity extends AppCompatActivity {

    private EditText editFilename;
    private EditText editURL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefs);
        Bundle extras = getIntent().getExtras();
        editFilename = (EditText) findViewById(R.id.editFileName);
        editURL = (EditText) findViewById(R.id.editURL);
        if (extras != null) {
            String filename = extras.getString("filename");
            String url = extras.getString("url");
            editFilename.setText(filename);
            editURL.setText(url);
        }
    }

    public void onSaveClick(View v){
        saveState();
        System.out.println("save!");
    }

    @Override
    public void onStop() {
        super.onStop();
        //saveState(); // Disabled because works only after restart
    }

    private void saveState() {
        String string = new String();
        try {
            string = new JSONStringer()
                    .object()
                    .key("WordsFileName")
                    .value(editFilename.getText())
                    .key("WordsFileDownloadURL")
                    .value(editURL.getText())
                    .endObject()
                    .toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(getResources().getString(R.string.settings_filename), Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}