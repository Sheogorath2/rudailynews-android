package org.spiralarms.rudailynews;

import android.annotation.TargetApi;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SyncAdapterType;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private File wordfile;
    private TextView textView;
    private String JSONURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);;

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        LoadSettings();
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        textView = (TextView) findViewById(R.id.text);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onResume(){
        super.onResume();
        LoadSettings();
        System.out.println(JSONURL);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void LoadSettings(){
        File settingsfile = new File(String.valueOf(getFileStreamPath(getResources().getString(R.string.settings_filename))));

        String wordfilename=getResources().getString(R.string.default_filename);
        String URL = getResources().getString(R.string.default_JSONURL);

        if (settingsfile.exists()) {
            String settingsjson =fileToString(settingsfile);

            JSONObject jobj= null;
            try {
                jobj = new JSONObject(settingsjson.toString());
                wordfilename = jobj.getString("WordsFileName");
                URL = jobj.getString("WordsFileDownloadURL");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        System.out.println(URL);
        JSONURL = URL;
        wordfile=new File(String.valueOf(getFileStreamPath(wordfilename)));
    }

    public String fileToString(File file) {
        String str = new String();
        FileInputStream inputStream;
        try {
            inputStream = openFileInput(file.getName());
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            str=sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public String getSentence(String json) {
        JSONObject jobj= null;
        try {
            jobj = new JSONObject(json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray jarray = null;
        ArrayList<String> subject = new ArrayList<String >();
        ArrayList<String> predicate = new ArrayList<String >();
        ArrayList<String> word3 = new ArrayList<String >();
        try {
            jarray=jobj.getJSONArray("subject");
            for (int i = 0; i < jarray.length(); i++) {
                subject.add(jarray.getString(i));

            }
            jarray=jobj.getJSONArray("predicate");
            for (int i = 0; i < jarray.length(); i++) {
                predicate.add(jarray.getString(i));

            }
            jarray=jobj.getJSONArray("word3");
            for (int i = 0; i < jarray.length(); i++) {
                word3.add(jarray.getString(i));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        int ns = (int) Math.floor(Math.random()*subject.size());
        int np = (int) Math.floor(Math.random()*predicate.size());
        int n3 = (int) Math.floor(Math.random()*word3.size());
        return (subject.get(ns)+" "+predicate.get(np)+" "+word3.get(n3));
    }

    public void onScreenClick(View v) {
        if (wordfile.exists()) {
            textView.setText(getSentence(fileToString(wordfile)));
        } else {
            textView.setText(R.string.NOFILE);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void onUpdateClick(View v) {

        textView.setText(R.string.CONNECTING);

        URL url = null;
        try {
            url = new URL(JSONURL);
        } catch (Exception e) {
                textView.setText(R.string.ERROR);
                e.printStackTrace();
                return;
        }

        String line;
        StringBuilder sb = new StringBuilder();
        // Create a URL for the desired page
        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            // Read all the text returned by the server
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
            textView.setText(R.string.NOCONNECTION);
            return;
        }
        catch (IOException e) {
            textView.setText(R.string.ERROR);
            e.printStackTrace();
            return;
        }

        String string = sb.toString();


        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(wordfile.getName(), Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            textView.setText(R.string.ERROR);
        } catch (IOException e) {
            textView.setText(R.string.ERROR);
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        textView.setText(R.string.FILE_NEW);

    }

    public void onCilpboardButtonClick(View v) {
        ClipboardManager _clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
        _clipboard.setText(textView.getText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, PrefsActivity.class);


            intent.putExtra("filename",wordfile.getName());
            intent.putExtra("url",JSONURL);

            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://org.spiralarms.rudailynews/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://org.spiralarms.rudailynews/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

}